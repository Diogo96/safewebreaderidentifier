﻿using System;
using System.Windows.Forms;
using PCSC;


// Build 2018 by Lucas Diogo da Silva

namespace ATRIdentifier
{
    public partial class Downloadform : Form
    {
        public Downloadform()
        {
            InitializeComponent();
        }
private void Refrash_Click(object sender, EventArgs e){

            try {
                var contextFactory = ContextFactory.Instance;
                using (var context = contextFactory.Establish(SCardScope.System))
                {
                    //Console.WriteLine("Leitores conectados: ");
                    var readerNames = context.GetReaders();
                    foreach (var readerName in readerNames){

                        // Caso utilize as portas dos tokens o mesmo não é reconhecido
                        if (readerName.Contains("AKS") || readerName.Contains("Rainbow"))
                            continue;

                        if ((context.GetReaderStatus(readerName).EventState.HasFlag(SCRState.Present))) {
                            var reader = context.ConnectReader(readerName, SCardShareMode.Shared, SCardProtocol.Any);
                            var cardAtr = reader.GetAttrib(SCardAttribute.AtrString);
                            string atr = "";
                            infodownload.Text = "";
                            progressBar2.Value = 0;
                            dados.Text = "";
                            porcentagem.Text = "0%";
                            atr = (BitConverter.ToString(cardAtr));

                            if (String.IsNullOrWhiteSpace(atr)){
                                status.Text = "Erro ao ler o cartão, limpe o chip e em seguida reinsira na leitora!";
                                start.Enabled = false;
                            }
                            else {
                                download.Text = "Clique aqui para efetuar o download do aplicativo";
                                textatr.Text = atr;
                                start.Enabled = true;
                                status.Text = "Cartão reconhecido pela leitora";
                            }
                        }
                    }

                    //Verificação para constatar se a leitora ou cartão estão presentes
                    int flag = 0;
                    foreach (var leitora in readerNames){
                        // Caso utilize as portas dos tokens o mesmo não é reconhecido
                        if (leitora.Contains("AKS") || leitora.Contains("Rainbow"))
                            continue;

                        if ((context.GetReaderStatus(leitora).EventState.HasFlag(SCRState.Present))){
                            flag = 1;
                        }
                    }

                    if (flag == 0){
                        MessageBox.Show("Mensagem de erro: cartão não identificado. \n Verifique se o dispositivo está conectado corretamente.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        start.Enabled = false;
                        status.Text = "Reconecte a leitora em outra porta USB caso a mesma esteja conectada,\n em seguida limpe o chip do cartão e clique novamente em identificar.";
                        textatr.Text = "";
                    }
                }
            }
            catch{
                MessageBox.Show("Leitora não identificada, conecte-a na USB. Caso a mesma esteja conectada, insira em outra entrada. Caso o problema persista, instale os drivers do fabricante e tente novamente.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            }

        public void start_Click(object sender, EventArgs e){    
        var atr = textatr.Text; // GET ATR
        DownloadControl run = new DownloadControl();
            try {     
              run.StartDownload(atr, this.progressBar2,this.porcentagem,this.dados);
            }
            catch
            {
            }



        }
    }
}



    

