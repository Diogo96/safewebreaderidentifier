﻿using System;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;
using System.Diagnostics;

// Build 2018 by Lucas Diogo da Silva
namespace ATRIdentifier
{
   public class DownloadControl
    {
        WebClient client;
        private static Label data;
        private static string verify;
        Stopwatch sw = new Stopwatch();
        private static Label downloaddata;
        Downloadform instance = new Downloadform();
        ServiceControl service = new ServiceControl();
        private static ProgressBar atualizarprogresso;

        public void StartDownload(string atr, ProgressBar atualizar,Label baixado, Label recebedados){

        client = new WebClient();
        client.DownloadFile(new Uri("https://safeweb.com.br/portals/10/downloads/oberthurlist.txt"), @"C:\Windows\Temp\oberthurlist.txt");// Faz o download do txt na temp
        string arch = System.Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE", EnvironmentVariableTarget.Machine); // Verifica a arquitetura do processador
        client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);// Acompanha a progressão do download
        client.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);// Notifica quando o download estiver finalizado

        string[] f = File.ReadAllLines(@"C:\Windows\Temp\oberthurlist.txt"); // Faz a leitura do TXT de ATR
        string tipo = atr.Replace("-", " "); // Remove os traços do ATR
        string found = ""; // Variavel de verificação
           
        atualizarprogresso = atualizar;
        data = baixado;
        downloaddata = recebedados;
          
          foreach (string line in f){
                //Procurar o ATR
                if (line == tipo){
                found = line;
                     if (arch == "AMD64" || arch== "IA64")
                    {
                     client.DownloadFileAsync(new Uri("https://safeweb.com.br/portals/10/downloads/AWP_5.1.8_SR1_Brazil_(User)64-bit.MSI"), @"C:\Windows\Temp\AWP64.msi");
                     verify = "AWP64";
                     }else{
                     client.DownloadFileAsync(new Uri("https://safeweb.com.br/portals/10/downloads/AWP_5.1.8_SR1_Brazil_(User)32-bit.MSI"), @"C:\Windows\Temp\AWP32.msi");
                     verify = "AWP32";}

                    }else if (found != tipo) {

                    if (arch == "AMD64" || arch == "IA64")
                    {
                    client.DownloadFileAsync(new Uri("https://safeweb.com.br/portals/10/downloads/Safesign-30112-x64.exe"), @"C:\Windows\Temp\Safesign64.exe");
                    verify = "Safesign64";
                    }
                    else{
                    client.DownloadFileAsync(new Uri("https://safeweb.com.br/portals/10/downloads/Safesign-30112-x86.exe"), @"C:\Windows\Temp\Safesign32.exe");
                    verify = "Safesign32";
                    }

                }
 
              }            
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e){
        // download speed
        // string speed = string.Format("{0} kb/s", (e.BytesReceived / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00"));
        // update na barra de progresso
        var progresso = e.ProgressPercentage;
        // Mostrar a porcentagem
        string porcentagem = e.ProgressPercentage.ToString() + "%";
        // update dos dados baixados 
        string dados = string.Format("{0} MB's / {1} MB's",(e.BytesReceived / 1024d / 1024d).ToString("0.00"),
        (e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));

        atualizarprogresso.Value = progresso;
        data.Text = porcentagem;
        downloaddata.Text = dados;
        }

        private void Completed(object sender, AsyncCompletedEventArgs e){
        // Reset the stopwatch.
        sw.Reset();
        if (e.Cancelled == true){
        MessageBox.Show("Download cancelado!");
        }else{
        MessageBox.Show("Download finalizado! Por favor aguarde até o aplicativo ser executado.");

                if(verify == "AWP64"){
                ExecuteControl.ExecuteOberthur64();
                }else if(verify == "AWP32"){
                ExecuteControl.ExecuteOberthur32();
                }else if(verify == "Safesign64"){
                ExecuteControl.ExecuteSafesign64();
                }else{
                ExecuteControl.ExecuteSafesign32();
                }
            }
        }



    }
}

