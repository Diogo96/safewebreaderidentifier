﻿
using System.Diagnostics;

namespace ATRIdentifier
{
    class ExecuteControl
    {

        private static string caminhoSafesign = @"C:\Program Files\A.E.T. Europe B.V\SafeSign\tokenadmin.exe";
        private static string caminhoOberthur = @"C:\Program Files (x86)\Oberthur Technologies\AWP\IdentityManager.exe";
        
        public static void ExecuteOberthur64()
        {
            try
            {
                using (Process exeProcess = Process.Start(@"C:\Windows\Temp\AWP64.msi"))
                {
                    exeProcess.WaitForExit();
                }
                ServiceControl.StartService();

                if (System.IO.File.Exists(caminhoOberthur))
                {
                     //Process.Start(caminhoOberthur);
                }
            }
            catch
            {

            }
        }

        public static void ExecuteOberthur32()
        {
            try
            {


                using (Process exeProcess = Process.Start(@"C:\Windows\Temp\AWP32.msi"))
                {
                    exeProcess.WaitForExit();
                }

                ServiceControl.StartService();
                if (System.IO.File.Exists(caminhoOberthur))
                {
                    //Process.Start(caminhoOberthur);
                }
            }
            catch
            {

            }
        }

        public static void ExecuteSafesign64()
        {

            try
            {
                using (Process exeProcess = Process.Start(@"C:\Windows\Temp\Safesign64.exe"))
                {
                    exeProcess.WaitForExit();
                }
                ServiceControl.StartService();

                if (System.IO.File.Exists(caminhoSafesign))
                {
                   Process.Start(caminhoSafesign);
                }
            }
            catch
            {

            }
        }

        public static void ExecuteSafesign32()
        {

            try
            {
                using (Process exeProcess = Process.Start(@"C:\Windows\Temp\Safesign32.exe"))
                {
                    exeProcess.WaitForExit();
                }
                ServiceControl.StartService();

                if (System.IO.File.Exists(caminhoSafesign))
                {
                    Process.Start(caminhoSafesign);
                }
            }
            catch
            {

            }


        }


    }
}
