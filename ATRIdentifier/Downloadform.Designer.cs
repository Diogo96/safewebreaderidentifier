﻿namespace ATRIdentifier
{
    partial class Downloadform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Downloadform));
            this.panel3 = new System.Windows.Forms.Panel();
            this.textatr = new System.Windows.Forms.TextBox();
            this.start = new System.Windows.Forms.Button();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.Refrash = new System.Windows.Forms.Button();
            this.status = new System.Windows.Forms.Label();
            this.download = new System.Windows.Forms.Label();
            this.infodownload = new System.Windows.Forms.Label();
            this.dados = new System.Windows.Forms.Label();
            this.porcentagem = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.textatr);
            this.panel3.Location = new System.Drawing.Point(71, 25);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(463, 59);
            this.panel3.TabIndex = 1;
            // 
            // textatr
            // 
            this.textatr.Enabled = false;
            this.textatr.Location = new System.Drawing.Point(36, 17);
            this.textatr.Name = "textatr";
            this.textatr.ReadOnly = true;
            this.textatr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textatr.Size = new System.Drawing.Size(403, 20);
            this.textatr.TabIndex = 0;
            // 
            // start
            // 
            this.start.Enabled = false;
            this.start.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.start.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.start.Location = new System.Drawing.Point(247, 221);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(123, 28);
            this.start.TabIndex = 2;
            this.start.Text = "Download";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(107, 147);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(403, 23);
            this.progressBar2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ATR: ";
            // 
            // Refrash
            // 
            this.Refrash.Location = new System.Drawing.Point(558, 35);
            this.Refrash.Name = "Refrash";
            this.Refrash.Size = new System.Drawing.Size(75, 32);
            this.Refrash.TabIndex = 5;
            this.Refrash.Text = "Identificar";
            this.Refrash.UseVisualStyleBackColor = true;
            this.Refrash.Click += new System.EventHandler(this.Refrash_Click);
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status.Location = new System.Drawing.Point(68, 87);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(0, 16);
            this.status.TabIndex = 6;
            // 
            // download
            // 
            this.download.AutoSize = true;
            this.download.Location = new System.Drawing.Point(193, 258);
            this.download.Name = "download";
            this.download.Size = new System.Drawing.Size(0, 13);
            this.download.TabIndex = 7;
            // 
            // infodownload
            // 
            this.infodownload.AutoSize = true;
            this.infodownload.Location = new System.Drawing.Point(106, 189);
            this.infodownload.Name = "infodownload";
            this.infodownload.Size = new System.Drawing.Size(0, 13);
            this.infodownload.TabIndex = 8;
            // 
            // dados
            // 
            this.dados.AutoSize = true;
            this.dados.Location = new System.Drawing.Point(255, 189);
            this.dados.Name = "dados";
            this.dados.Size = new System.Drawing.Size(0, 13);
            this.dados.TabIndex = 9;
            // 
            // porcentagem
            // 
            this.porcentagem.AutoSize = true;
            this.porcentagem.Location = new System.Drawing.Point(531, 156);
            this.porcentagem.Name = "porcentagem";
            this.porcentagem.Size = new System.Drawing.Size(21, 13);
            this.porcentagem.TabIndex = 10;
            this.porcentagem.Text = "0%";
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.Location = new System.Drawing.Point(0, 288);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(673, 19);
            this.panel4.TabIndex = 11;
            // 
            // Downloadform
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.ClientSize = new System.Drawing.Size(663, 304);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.porcentagem);
            this.Controls.Add(this.dados);
            this.Controls.Add(this.infodownload);
            this.Controls.Add(this.download);
            this.Controls.Add(this.status);
            this.Controls.Add(this.Refrash);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.start);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.MaximizeBox = false;
            this.Name = "Downloadform";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gerenciador de download";
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

      
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textatr;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Refrash;
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.Label download;
        public System.Windows.Forms.ProgressBar progressBar2;
        public System.Windows.Forms.Label infodownload;
        private System.Windows.Forms.Label dados;
        private System.Windows.Forms.Label porcentagem;
        private System.Windows.Forms.Panel panel4;
    }
}